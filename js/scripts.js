var bigBug = $('#bigBug'),
    i = 1,
    q = localStorage.getItem('q'),
    $timer = document.getElementById("timer"),
    $ammoCont = document.getElementById('ammoContainer'),
    $easy = $('.easy'),
    $medium = $('.medium'),
    $hard = $('.hard'),
    $lady1 = $('#lady1'),
    $lady2 = $('#lady2'),
    $lady3 = $('#lady3'),
    $hint1 = $('#hint1'),
    $hint2 = $('#hint2'),
    $hint3 = $('#hint3'),
    rotation = 0,
    ammo = false;
    rotationX = 0;

//var dictionary = Object.create(null);    ***************** dobra rzecz polecam Marek

function getRandom(maxSize) {
    return parseInt(Math.random() * maxSize);
}

if (q == undefined) {
    q = 100;
}

function hints(elem){
    $(elem).css('display', 'table')
        .animate({
        opacity: 0,
    }, 3000)

}

TweenMax.staggerTo(".hint", 1, {rotation:360, ease: Elastic.easeOut.config(1, 0.3)}, 2);

function showHints () {
    setTimeout('hints($hint1);', 300);
    setTimeout('hints($hint2);', 2300);
    setTimeout('hints($hint3);', 4300);
}

showHints();

function play(q, elem) {
    localStorage.setItem('q', q);
    spinX(elem);
    setInterval(function () {
        window.location.href = 'game.html';
    }, 1000);
    var audio = new Audio('sounds/1.wav');
    audio.play();
}

function spinX(element) {
    rotationX += 360;
    TweenLite.to(element, 2, {rotationX: rotationX, ease: Power2.easeOut});
}

function pulse(element) {
    TweenMax.to(element, 1.2, {
        scaleX: 0.8,
        scaleY: 0.8,
        force3D: true,
        yoyo: true,
        repeat: -1,
        ease: Power1.easeInOut
    });
}

pulse($easy);
pulse($medium);
pulse($hard);

spin($easy, $lady1);
spin($medium, $lady2);
spin($hard, $lady3);

function spin(elementHover, elementToSpin) {
    var ignoreRollovers;
    elementHover.hover(function () {
        var audio = new Audio('sounds/magic.wav');
        audio.play();
        if (!ignoreRollovers) {
            rotation += 360;
            ignoreRollovers = true;
            TweenLite.to(elementToSpin, 1.5, {rotation: rotation, ease: Power4.easeOut});
            TweenLite.delayedCall(1, function () {
                ignoreRollovers = false;
            });
        }
    }, function () {
    });
}

// ************* shootingEffect function **************
function shootingEffect() {
    $(window).click(function () {
        if (ammo === false) {
            $(window).click(function () {
                var audio = new Audio('sounds/error.wav');
                audio.play();
            });
        } else {
            if (ammo > 0) {
                ammo--;
                i = getRandom(3) + 1;
                var audio = new Audio('sounds/' + i + '.wav');
                audio.play();
                $('#ammo').html('Amunicja: ' + ammo);
                $ammoCont.removeChild($ammoCont.children[0]);
            } else if (ammo == 0) {
                $($ammoCont).html('<p id="loading">Ładuj!!! (kliknij tu lub spacja)</p>');
                var $loading = $('#loading');
                pulse($loading);
                $(window).unbind("click");
                $(window).click(function (e) {
                    var audio = new Audio('sounds/error.wav');
                    audio.play();
                    createInfo('SPACJA!', e);
                });
            } else {
                $(window).unbind("click");
                $(window).click(function (e) {
                    var audio = new Audio('sounds/error.wav');
                    audio.play();
                    createInfo('ładowanie...', e);
                });
            }
        }
    });

}
shootingEffect();


//********************************** creating ammunition *******************

var createRound = function() {
    var $round = document.createElement("div");
    $round.className = "round";
    $ammoCont.appendChild($round);
};

var createAmmo = function() {
    $($ammoCont).html('');
    for(let i = 0; i<q/8; i++){
        createRound();
    }
};

// ************* reloading function **************

var reloading = function(){
    ammo = q/8;
    $(window).unbind("click");
    shootingEffect();
    var audio = new Audio('sounds/reloadComplete.wav');
    audio.play();

};

var reload = function(){
    // subtracting ammo for avoiding bug
    ammo--;
    createAmmo();
    shootingEffect();
    var audio = new Audio('sounds/reload.wav');
    audio.play();
    setTimeout(reloading, 1500);
    spinX($ammoCont);
};

$(window).keypress(function(e) {
    if (e.which === 32 && ammo == 0) {
        reload();
    }
});

$($ammoCont).click(function() {
    if (ammo == 0) {
        reload();
    }
});











